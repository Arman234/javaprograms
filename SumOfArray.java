package com.programs;

public class SumOfArray {
	public static int sumOfArray(int[] arr, int size) {
		int sum = 0;
		for(int i = 0; i < size; i++) {
			sum += arr[i];
		}
		return sum;
	}
	public static void main(String[] args) {
		int[] arr = {10, 20, 30, 40, 50};
		int size = arr.length;
		int sum = sumOfArray(arr, size);
		System.out.println("Sum of Array is : "+sum);
	}
}
