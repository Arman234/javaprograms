package com.programs;

public class ReverseOfNumber {
	public static int reverseNum(int num) {
		int rev = 0;
		int rem = 0;
		while(num>0) {
			rem = num % 10;
			rev = (rev*10) + rem;
			num = num / 10;
		}
		return rev;
	}
	public static void main(String[] args) {
		int num = 3728828;
		int rev = reverseNum(num);
		System.out.println("Reverse Number is : "+rev);
	}
}
