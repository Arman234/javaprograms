package com.programs;

public class DisplayFibonacci {
	public static void fibonacci(int size) {
		int a = 0;
		int b = 1;
		System.out.print(a+" "+b);
		for(int i = 0; i < size-2; i++)
		{
			int c = a + b;
			System.out.print(" "+c);
			a = b;
			b = c;
		}
	}
	public static void main(String[] args) {
		int size = 12;
		fibonacci(size);
	}
}
