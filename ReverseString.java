package com.programs;

public class ReverseString {
	public static String reverseString(String str) {
		String temp = "";
		for(int i=0; i<str.length(); i++) {
			temp = str.charAt(i)+temp;
		}
		return temp;
	}
	public static void main(String[] args) {
		String str = "Arman";
		String reverseString = reverseString(str);
		System.out.println("Reverse String is : "+reverseString);
	}
}
