package com.programs;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class UniqueString {  
	public static void printStringWithOccurrences(String[] str, int size) {
		Map<String, Integer> map = new LinkedHashMap<>();
		int i = 0;
		while(i != size) {
			if(!map.containsKey(str[i])) {
				map.put(str[i], 1);
			}
			else{
				int oldVal = map.get(str[i]);
				int newVal = oldVal + 1;
				map.put(str[i], newVal);
			}
			i++;
		}
		Set<Map.Entry<String, Integer>> lmap = map.entrySet();
		for(Map.Entry<String, Integer> data : lmap) {
			System.out.print(data.getKey()+" : "+data.getValue());
			System.out.println();
		}
	}
	public static void main(String[] args) {
		String[] str = {"Arman", "Raktim", "Arka", "Arman", "Sourav", "Arkajit"};
		int size = str.length;
		printStringWithOccurrences(str, size);
	}
}
