package com.programs;

public class FindFactorial {
	public static int factorialOfNum(int num) {
		int fact = 1;
		if(num == 0)
			return 1;
		for(int i = 1; i <= num; i++) {
			 fact *= i;
		}
		return fact;
	}
	public static void main(String[] args) {
		int num = 5;
		int fact = factorialOfNum(num);
		System.out.println("Factorial of given number is : "+fact);
	}
}
