package com.programs;


public class NthLargestNumber { //// Incomplete
	public static int findNthLargestNumber(int[] arr, int size, int Nth) {
		
		SortNumbers.sortNumbers(arr, size);
		// nth Largest
		return arr[size-Nth-1];
		
		// nth Smallest
//		return arr[Nth-1];
	}
	public static void main(String[] args) {
		int[] arr = {10 ,30 ,80 ,48 ,3 ,47 ,56 ,34};
		int size = arr.length;
		int Nth = 5;
		int num = findNthLargestNumber(arr, size, Nth);
		System.out.println("Nth Largest Number is : "+num);
	}
}
