package com.programs;

public class SortNumbers {
	public static int[] sortNumbers(int[] arr, int size) {
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				int temp;
				if (arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr;
	}

	public static void main(String[] args) {
		int[] arr = { 10, 3, 54, 72, 5, 8 };
		int size = arr.length;
		int[] sortedArray = sortNumbers(arr, size);
		// For print array
		for (int res : sortedArray) {
			System.out.print(res + " ");
		}

	}
}
