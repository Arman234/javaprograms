package com.programs;

public class LargestNumber {
	public static int getLargestNumber(int num1, int num2, int num3) {
		int res = (num1 > num2)?(num1 > num3?num1:num3):(num2 > num3 ? num2:num3);
		return res;
	}
	public static void main(String[] args) {
		int num1 = 20;
		int num2 = 30;
		int num3 = 10;
		int out = getLargestNumber(num1, num2, num3);
		System.out.println("Largest number is : "+out);
	}
}
