package com.programs;

public class LeapYear { 
	public static String isLeapYear(int year) {
		if((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))
			return "Leap Year";
		else
			return "not a Leap Year";
	}
	public static void main(String[] args) {
		int year = 2000;
		String res = isLeapYear(year);
		System.out.println(year+" is "+res);
	}
}
