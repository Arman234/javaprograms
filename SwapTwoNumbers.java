package com.programs;

public class SwapTwoNumbers {
	public static void swappingNumbers(int num1, int num2) {
		int temp ;
		System.out.println("Before Swapping : "+num1+" "+num2);
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("After Swapping : "+num1+" "+num2 );
	}
	public static void main(String[] args) {
		int num1 = 10;
		int num2 = 20;
		swappingNumbers(num1, num2);
	}
}
