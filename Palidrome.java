package com.programs;

public class Palidrome {
	public static String checkPalindrome(String str) {
		String revsString = ReverseString.reverseString(str);
		String res = str.equals(revsString)?"Palindrome":"Not";
		return res;
	}
	public static void main(String[] args) {
		String str = "liril";
		String isPalindrome = checkPalindrome(str);
		System.out.println("Given String is : "+isPalindrome);
	}
}
