package com.programs;

public class CheckEvenOrOdd {
	public static String checkEvenOrOdd(int num) {
		if(num == 0)
			return 0+" is not even not odd";
		String res = num % 2 == 0 ? "Even":"Odd";
		return res;
	}
	public static void main(String[] args) {
		int num = 23;
		String res = checkEvenOrOdd(num);
		System.out.println("Number is : "+res);
	}
}
