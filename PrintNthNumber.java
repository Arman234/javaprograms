package com.programs;

public class PrintNthNumber {
	public static String getNthNumber(String str, int nth) {
		str = str.replaceAll("[^0-9 ]", "");
		str = str.trim();
		str = str.replaceAll(" +", " ");
//		System.out.println(str);
		String[] arr = str.split(" ");
		int count = 1;
		for (int i = 0; i < arr.length; i++) {
			if(count != nth)
				count++;
			else if(count == nth) 
				return arr[i];
		}
		return "There is no number available";
	}

	public static void main(String[] args) {
		String str = "This is ab1245 playing 65 football 3 highest 7 goal 12";
		int nth = 2;
		String out = getNthNumber(str, nth);
		System.out.println("Output is : "+out);
	}
}
