package com.programs;

public class CheckPrimeNumber {
	public static String isPrime(int num) {
		if(num < 0)
			return "Please give any Positive number";
		else if(num == 0 || num == 1)
			return "Neither Prime nor non_prime";
		
		for(int i = 2; i < num ; i++) {
			if(num % i == 0) {
				return "Not Prime";
			}
		}
		return "Prime Number";
	}
	public static void main(String[] args) {
		int num = 8;
		String isPrime = isPrime(num);
		System.out.println("Given number is : "+isPrime);
	}
}
